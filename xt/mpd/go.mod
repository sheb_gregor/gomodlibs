module gitlab.com/sheb_gregor/gomodlibs/xt/mpd

go 1.17

require (
	github.com/spf13/cobra v1.3.0
	gitlab.com/sheb_gregor/gomodlibs/v3 v3.0.0
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)

replace gitlab.com/sheb_gregor/gomodlibs/v3 => ../../
