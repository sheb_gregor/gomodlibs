package mpd

import (
	"github.com/spf13/cobra"
	"gitlab.com/sheb_gregor/gomodlibs/v3"
)

func AddSelf() {
	gomodlibs.Add("mpd", New)
}

type Player struct {
	name string
}

func New(name string, opts []interface{}) gomodlibs.IPlayer {
	var rootCmd = &cobra.Command{
		Use:   "hugo",
		Short: "Hugo is a very fast static site generator",
		Long: `A Fast and Flexible Static Site Generator built with
                love by spf13 and friends in Go.
                Complete documentation is available at http://hugo.spf13.com`,
		Run: func(cmd *cobra.Command, args []string) {
			// Do Stuff Here
		},
	}
	_ = rootCmd
	return &Player{name: name}
}

func (p *Player) Play() bool {
	return true
}

func (p *Player) Name() string {
	return p.name
}
