package foobar

import (
	"gitlab.com/sheb_gregor/gomodlibs/v3"
	"testing"
)

func TestIPlayer(t *testing.T) {
	AddSelf()
	p := gomodlibs.New("foobar", "test_1", nil)
	if !p.Play() {
		t.Fatal("does not work")
	}
	if p.Name() != "test_1" {
		t.Fatal("does not match")
	}
}
