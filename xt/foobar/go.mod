module gitlab.com/sheb_gregor/gomodlibs/xt/foobar

go 1.17

require (
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/sheb_gregor/gomodlibs/v3 v3.0.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
)

replace gitlab.com/sheb_gregor/gomodlibs/v3 => ../../
