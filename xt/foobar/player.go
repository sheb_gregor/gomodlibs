package foobar

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/sheb_gregor/gomodlibs/v3"
)

func AddSelf() {
	gomodlibs.Add("foobar", New)
}

type Player struct {
	name string
}

func New(name string, opts []interface{}) gomodlibs.IPlayer {
	_ = cli.NewApp()
	return &Player{name: name}
}

func (p *Player) Play() bool {
	return true
}

func (p *Player) Name() string {
	return p.name
}
