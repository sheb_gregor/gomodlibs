package gomodlibs

import (
	"testing"
)

type tp string

func (i tp) Name() string { return string(i) }
func (i tp) Play() bool   { return true }

func TestIPlayer(t *testing.T) {
	Add("test_1", func(name string, opt []interface{}) IPlayer { return tp(name) })
	p := New("test_1", "test_1", nil)
	if !p.Play() {
		t.Fatal("does not work")
	}
	if p.Name() != "test_1" {
		t.Fatal("does not match")
	}
}
