package gomodlibs

import (
//	"gitlab.com/sheb_gregor/gomodlibs/v2/foobar"
)

var drivers = map[string]func(string, []interface{}) IPlayer{}

type IPlayer interface {
	Play() bool
	Name() string
}

func New(driver, name string, opts []interface{}) IPlayer {
	return drivers[driver](name, opts)
}

func Add(driver string, f func(string, []interface{}) IPlayer) {
	drivers[driver] = f
}
