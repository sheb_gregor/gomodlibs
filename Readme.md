# Go.mod sandbox

This is an example repository.
It was used for experiments with go (sub)modules.

Here is a code of sandbox project, that uses this lib.

- main.go 

```go 
package main

import (
	"gitlab.com/sheb_gregor/gomodlibs"
	v3 "gitlab.com/sheb_gregor/gomodlibs/v3"
	"gitlab.com/sheb_gregor/gomodlibs/xt/foobar"
	"gitlab.com/sheb_gregor/gomodlibs/xt/mpd"
)

func main() {
	player := gomodlibs.New("foobar", "test_1")
	println(player.Name())

	foobar.AddSelf()
	mpd.AddSelf()
	pv2 := v3.New("foobar", "test_foobar", []interface{}{})
	println(pv2.Name())

	pv2 = v3.New("mpd", "test_mpd", []interface{}{})
	println(pv2.Name())
}

```

- go.mod

```gomodlibs
module golibtest

go 1.17

require (
	gitlab.com/sheb_gregor/gomodlibs v1.1.0
	gitlab.com/sheb_gregor/gomodlibs/v3 v3.1.2
	gitlab.com/sheb_gregor/gomodlibs/xt/foobar v1.0.0
	gitlab.com/sheb_gregor/gomodlibs/xt/mpd v1.0.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/spf13/cobra v1.3.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/urfave/cli/v2 v2.3.0 // indirect
)

```
